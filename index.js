
const FIRST_NAME = "Teodor";
const LAST_NAME = "Popescu";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function numberParser(x) {
    // if (typeof (x) == "number") 
    // 	return Math.floor(x);

    // if (typeof x == "string")
    // 	return parseInt(x);

    // if (isNaN(x) == true) 
    // 	return NaN;

    // if (x === Infinity || x === -Infinity)
    // 	return NaN;
    	
    // 	    if ((x > Number.MAX_SAFE_INTEGER) || (x < Number.MIN_SAFE_INTEGER))
    // 	    	return NaN;
    if(x===NaN)
    	return NaN;
    if(x===Infinity||x===-Infinity)
    	return NaN;
    if(x>Number.MAX_SAFE_INTEGER||x<Number.MIN_SAFE_INTEGER)
    	return NaN;

    return Number.parseInt(x);
}

console.log(numberParser(56.3));
console.log(numberParser('56.3'));
console.log(numberParser(NaN));
console.log(numberParser(Infinity));

function dynamicPropertyChecker(input, property) {
    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

